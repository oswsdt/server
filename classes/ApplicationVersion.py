from classes import Tools


class ApplicationVersion:
    def __init__(self, data):
        self.id = data[0]
        self.application_id = data[1]
        self.architecture = Tools.getArchitectureString(data[2])
        self.version = data[3]
        self.wversion = data[4]
        self.path = data[5]

    def getID(self):
        return self.id

    def getArchitecture(self):
        return self.architecture

    def getVersion(self):
        return self.version

    def getWVersion(self):
        return self.wversion

    def getPath(self):
        return self.path

    def getApplicationID(self):
        return self.path
