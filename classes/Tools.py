from classes import Device

# returns DeviceArchitecture
def getArchitectureByString(raw):
    if raw == 0 or raw == "0" or raw == "x86" or raw == "32" or raw == "32bit":
        return Device.DeviceArchitecture.x86
    if raw == 0 or raw == "2" or raw == "arm" or raw == "arm64":
        return Device.DeviceArchitecture.arm64
    # fallback, default
    return Device.DeviceArchitecture.x64


def safeString(a):
    if isinstance(a, str):
        return a.replace("'", "").replace("\"", "").replace("`", "").replace("\\", "")
    return a


def getArchitectureStringByInt(number):
    if number == 1:
        return "x86"
    if number == 2:
        return "x64"
    return "arm64"


def getArchitectureString(architecture):
    if architecture == Device.DeviceArchitecture.x86:
        return "x86"
    if architecture == Device.DeviceArchitecture.x64:
        return "x64"
    return "arm64"
