import sys

import yaml

from classes import Log


def loadConfig(config_file):
    file = open(config_file, "r")
    config = yaml.load(file, Loader=yaml.FullLoader)
    file.close()
    fields = ['host', 'port', "mysql-host", "mysql-user", "mysql-db", "mysql-password", "log-to-file"]
    for field in fields:
        try:
            if config[field]:
                continue
        except yaml.YAMLError:
            Log.critical("Error reading config.yml")
            sys.exit(1)
    return config
