from classes import Storage


class Rollout:
    def __init__(self, data):
        self.id = data[0]
        self.application = Storage.applications.get(data[1])
        self.version = Storage.application_versions.get(data[2])
        self.groupids = []
        for groupid in data[3].split(","):
            self.groupids.append(int(groupid))

    def getID(self):
        return self.id

    def getApplication(self):
        return self.application

    def getVersion(self):
        return self.version

    def getGroupIDs(self):
        return self.groupids
