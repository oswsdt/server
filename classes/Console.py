from classes import Log
from classes import Storage


def shutdown():
    Log.info("Shutting down...")
    exit(0)


def shutdownAllDevices():
    for device in Storage.devices:
        device = Storage.devices.get(device)
        device.forceShutdown()


def rebootAllDevices():
    for device in Storage.devices:
        device = Storage.devices.get(device)
        device.forceReboot()


def gpUpdateAllDevices():
    for device in Storage.devices:
        device = Storage.devices.get(device)
        device.forceGPUpdate()


def testToAllDevices():
    for device in Storage.devices:
        device = Storage.devices.get(device)
        device.test()


def updateAllDevices():
    for device in Storage.devices:
        device = Storage.devices.get(device)
        device.update()


def rawInput(i):
    # raw command
    # split
    s = i.split(" ")
    if s[0] == "stop" or s[0] == "exit":
        shutdown()
    elif s[0] == "shutdown":
        shutdownAllDevices()
        Log.info("Force shutting down all devices")
    elif s[0] == "reboot":
        rebootAllDevices()
        Log.info("Force rebooting all devices")
    elif s[0] == "gpupdate":
        gpUpdateAllDevices()
        Log.info("Force gpupdating all devices")
    elif s[0] == "testall":
        testToAllDevices()
        Log.info("Running Device::test on all devices")
    elif s[0] == "update":
        updateAllDevices()
        Log.info("Checking for software updates on all devices...")

    else:
        Log.info("Unknown command!")
