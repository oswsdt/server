import socket
import ssl
from _thread import *

import server
from classes import Device
from classes import DeviceHandler
from classes import Log
from classes import MySQL
from classes import PackageUtil
from classes import Protocol


# handles clients
def handleClient(s, h, p):
    # get client id by certificate
    device = MySQL.getClientByCert(ssl.DER_cert_to_PEM_cert(s.getpeercert(binary_form=True)))
    try:
        if device.getConnectionStatus() == Device.ConnectionStatus.CONNECTED:
            Log.warn("Client %s from %s: %s is already connected! Kicking..." % (device.getName(), h, p))
            s.close()
            return
        device.setConnectionStatus(Device.ConnectionStatus.CONNECTED)
        Log.client('Client connected: %s from %s: %s' % (device.getName(), h, p))
    except TypeError:
        # will probably never occur
        Log.warn("Unknown client connected!")
        # can not continue
        s.close()
        return

    s.settimeout(1)  # 1 ms timeout for listening

    while True:
        if len(device.queue) == 0:
            # nothing to send, listen
            try:
                data = s.recv(Protocol.MAX_PACKAGE_LEN)
                if not data:
                    # broken pipe, timeout or disconnect
                    break
                json_data = PackageUtil.getJSONFromBytes(data)
                # debug
                print(json_data)
                # process packages
                DeviceHandler.handle(device, json_data)
            except socket.timeout:
                # is okay, continue reading and checking if something is to send...
                pass
            except socket.error:
                # probably disconnect
                break
        else:
            # data need to get sent
            for package in device.queue:
                s.sendall(package)
                device.queue.remove(package)
    device.setConnectionStatus(Device.ConnectionStatus.DISCONNECTED)
    Log.client('Client disconnected: %s: %s' % (h, p))


# create and bind socket
def buildSocket(config):
    # create
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    s.bind((config['host'], config['port']))
    s.listen(3)
    # ssl context
    context = ssl.create_default_context(ssl.Purpose.SERVER_AUTH, capath=server.CONFIG_FOLDER + "certs")
    context.load_cert_chain(certfile=server.CONFIG_FOLDER + "server.crt.pem",
                            keyfile=server.CONFIG_FOLDER + "server.key.pem")
    context.check_hostname = False
    context.verify_mode = ssl.CERT_REQUIRED

    # create ssl socket
    ws = context.wrap_socket(s, server_side=True)

    while True:
        # waiting for client
        try:
            client_socket, (h, p) = ws.accept()
            if not client_socket:
                Log.critical("Error in socket connection!")
            # new client: handle in own thread
            start_new_thread(handleClient, (client_socket, h, p))
        except socket.error:
            Log.info("Socket accept error")
