from base64 import b64encode

import pymysql

from classes import Application
from classes import ApplicationVersion
from classes import Device
from classes import Group
from classes import Installer
from classes import Log
from classes import Parameter
from classes import Rollout
from classes import Storage
from classes import Tools
from classes import WNameFormat

db = None


def connect(config):
    global db
    db = pymysql.connect(config['mysql-host'], config['mysql-user'],
                         config['mysql-password'], config['mysql-db'])


def getClientByCert(cert):
    global db
    c = db.cursor()
    c.execute("SELECT * FROM `auth` WHERE `cert` = '%s'" % b64encode(cert.encode('ascii')).decode('ascii'))
    did = c.fetchone()[0]
    if Storage.devices.get(did) is None:
        # device (data) never fetched
        Storage.devices.update({did: fetchDevice(did)})
    return Storage.devices.get(did)


def fetchDevice(did):
    Log.info("Fetching device info (%s)..." % str(did))
    # fetch device info
    c = db.cursor()
    c.execute("SELECT * FROM `devices` WHERE `id` = '%s'" % str(did))
    return Device.Device(c.fetchone())


def saveDevice(device):
    Log.info("Saving device info (%s)..." % str(device.getName()))
    c = db.cursor()
    c.execute(
        "UPDATE `devices` SET `name`='%s',`os`='%s',`architecture`='%s',`ram`='%s',`domain`='%s' WHERE `id` = '%s';" % (
            Tools.safeString(device.getName()), Tools.safeString(device.getOS()),
            Tools.getArchitectureString(device.getArchitecture()),
            Tools.safeString(device.getMemory()), Tools.safeString(device.getDomain()),
            Tools.safeString(device.getID())))
    db.commit()


def fetchGroup(gid):
    Log.info("Fetching group info (%s)..." % str(gid))
    # fetch group info
    c = db.cursor()
    c.execute("SELECT * FROM `groups` WHERE `id` = '%s'" % str(gid))
    return Group.Group(c.fetchone())


def fetchAllGroups():
    Log.info("Fetching all group info...")
    # fetch all group info
    c = db.cursor()
    c.execute("SELECT * FROM `groups`")
    groups = {}
    for group in c.fetchall():
        groups[group[0]] = Group.Group(group)  # index 0 => group id
    return groups


def fetchAllApplications():
    Log.info("Fetching all application info...")
    # fetch all group info
    c = db.cursor()
    c.execute("SELECT * FROM `applications`")
    applications = {}
    for application in c.fetchall():
        applications[application[0]] = Application.Application(application)  # index 0 => application id
    return applications


def fetchAllWNameFormats():
    Log.info("Fetching all wname formats info...")
    # fetch all wname formats
    c = db.cursor()
    c.execute("SELECT * FROM `wname_formats`")
    formats = {}
    for format in c.fetchall():
        formats[format[0]] = WNameFormat.WNameFormat(format)  # index 0 => format id
    return formats


def fetchAllInstallers():
    Log.info("Fetching all installers...")
    # fetch all installers
    c = db.cursor()
    c.execute("SELECT * FROM `installer`")
    installers = {}
    for installer in c.fetchall():
        installers[installer[0]] = Installer.Installer(installer)  # index 0 => format id
    return installers


def fetchAllParameters():
    Log.info("Fetching all parameters...")
    # fetch all parameters
    c = db.cursor()
    c.execute("SELECT * FROM `parameter`")
    parameters = {}
    for parameter in c.fetchall():
        parameters[parameter[0]] = Parameter.Parameter(parameter)  # index 0 => format id
    return parameters


def fetchAllApplicationVersions():
    Log.info("Fetching all versions...")
    # fetch all versions
    c = db.cursor()
    c.execute("SELECT * FROM `application_versions`")
    versions = {}
    for version in c.fetchall():
        versions[version[0]] = ApplicationVersion.ApplicationVersion(version)  # index 0 => format id
    return versions


def fetchAllRollouts():
    Log.info("Fetching all rollouts...")
    # fetch all rollouts
    c = db.cursor()
    c.execute("SELECT * FROM `rollouts`")
    rollouts = {}
    for rollout in c.fetchall():
        rollouts[rollout[0]] = Rollout.Rollout(rollout)  # index 0 => format id
    return rollouts
