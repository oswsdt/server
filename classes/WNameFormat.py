class WNameFormat:
    def __init__(self, data):
        self.id = data[0]
        self.format = data[1]

    def getID(self):
        return self.id

    def getFormat(self):
        return self.format
