import json

import server
from classes import Protocol


# builds a Package from JSON String
def getBytesFromJSON(data):
    string = json.dumps(data)

    response = bytearray(string, 'utf8')
    # return bytes
    return response


# builds a Package from JSON String
def getJSONFromBytes(raw):
    json_string = raw.decode('utf-8')
    # parse json
    parsed = json.loads(json.loads(json_string))  # parse it twice else it would just be an json string. ToTo better
    # return it
    return parsed


# this functions adds meta data, ... to the array
# it returns a json object
def preparePackage(data):
    final = {"data": data}
    meta = {"protocol-version": Protocol.VERSION, "version": server.VERSION}
    final["meta"] = meta
    return json.dumps(final)


def getBlankCommand(id, command):
    data = {id: {"id": id}}
    data[id]["command"] = command
    data[id]["data"] = {}

    return preparePackage(data)
