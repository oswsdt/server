from classes import Storage


class Installer:
    def __init__(self, data):
        self.id = data[0]
        self.name = data[1]
        self.parameters = []
        for parameter in data[2].split(","):
            self.parameters.append(Storage.parameters.get(int(parameter)))

    def getID(self):
        return self.id

    def getName(self):
        return self.name

    def getParameters(self):
        return self.parameters
