from classes import Storage


class Group:
    def __init__(self, data):
        self.id = data[0]
        self.name = data[1]
        self.description = data[2]
        self.rollouts = []
        for rollout in Storage.rollouts.values():
            if self.id in rollout.getGroupIDs():
                self.rollouts.append(rollout)

    def getID(self):
        return self.id

    def getName(self):
        return self.name

    def getDescription(self):
        return self.description

    def getRollouts(self):
        return self.rollouts
