from OpenSSL import crypto

import server
from classes import Log


def createCerts():
    # create a key pair
    k = crypto.PKey()
    k.generate_key(crypto.TYPE_RSA, 4096)

    # create a self-signed cert
    cert = crypto.X509()
    cert.get_subject().C = "DE"
    cert.get_subject().ST = "Munich"
    cert.get_subject().L = "Munich"
    cert.get_subject().O = "OSWSDT Server"
    cert.get_subject().OU = "OSWSDT Server"
    cert.get_subject().CN = "0.0.0.0"
    cert.set_serial_number(1000)
    cert.gmtime_adj_notBefore(0)
    cert.gmtime_adj_notAfter(10 * 365 * 24 * 60 * 60)
    cert.set_issuer(cert.get_subject())
    cert.set_pubkey(k)
    cert.sign(k, 'sha1')

    with open(server.CONFIG_FOLDER + "server.crt.pem", "wb") as f:
        f.write(crypto.dump_certificate(crypto.FILETYPE_PEM, cert))
        f.close()
    with open(server.CONFIG_FOLDER + "server.key.pem", "wb") as f:
        f.write(crypto.dump_privatekey(crypto.FILETYPE_PEM, k))
        f.close()
    Log.info("Server certificate generated.")


def setup():
    files = {"config.yml", "server.crt.pem", "server.key.pem"}
    for file in files:
        try:
            f = open(server.CONFIG_FOLDER + file)
            f.close()
        except IOError:
            if file == "config.yml":
                Log.critical(
                    "config.yml file does not exist! Please install the server first!")
                exit(1)
            else:
                # generate server certificates
                createCerts()
