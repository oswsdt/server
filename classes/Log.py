import datetime
from enum import Enum

import server


class LogLevel(Enum):
    INFO = 1,
    AUTHENTICATION = 2,
    WARNING = 3,
    CRITICAL = 4,
    DEBUG = 5,
    CLIENTS = 6
    SOCKET = 7


# log: LogLevel and message
def log(lvl, message):
    logText = '[' + lvlToString(lvl) + "] [" + \
              datetime.datetime.now().strftime("%d.%m. %Y - %H:%M %S") + "]: " + \
              message

    print(logText)

    # log to config/logs/serverlog.log if needed
    try:
        if server.config['log-to-file']:
            with open(server.CONFIG_FOLDER + "logs/serverlog.log", "a+") as logfile:
                logfile.write(logText + "\n")
    except TypeError:
        # not possible
        pass


def lvlToString(lvl):
    levels = {
        LogLevel.INFO: "INFO",
        LogLevel.AUTHENTICATION: "AUTHENTICATION",
        LogLevel.WARNING: "WARNING",
        LogLevel.CRITICAL: "CRITICAL",
        LogLevel.DEBUG: "DEBUG",
        LogLevel.CLIENTS: "CLIENTS",
        LogLevel.SOCKET: "SOCKET"
    }
    return levels.get(lvl, "OTHER")


# log methods

def info(m):
    log(LogLevel.INFO, m)


def auth(m):
    log(LogLevel.AUTHENTICATION, m)


def warn(m):
    log(LogLevel.WARNING, m)


def critical(m):
    log(LogLevel.CRITICAL, m)


def debug(m):
    log(LogLevel.DEBUG, m)


def client(m):
    log(LogLevel.CLIENTS, m)


def socket(m):
    log(LogLevel.CLIENTS, m)
