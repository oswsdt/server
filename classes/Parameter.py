class Parameter:
    def __init__(self, data):
        self.id = data[0]
        self.name = data[1]
        self.description = data[2]
        self.value = data[3]

    def getID(self):
        return self.id

    def getName(self):
        return self.name

    def getDescription(self):
        return self.description

    def getValue(self):
        return self.value
