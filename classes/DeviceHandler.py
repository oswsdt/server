from classes import Log
from classes import Tools


def handle(device, data):
    try:
        for cmd in data["data"].values():
            print("Command id=%s requested" % (cmd["command"]))
            if cmd["command"] is ClientToServerCommand.SEND_GENERAL_INFO_TO_SERVER:
                # update device info
                device.setOS(cmd["data"]["os"])
                device.setArchitecture(Tools.getArchitectureByString(cmd["data"]["architecture"]))
                device.setDeviceName(cmd["data"]["device-name"])
                device.setMemory(cmd["data"]["memory"])
                device.setDomain(cmd["data"]["domain"])
                device.saveProperties()
                print("Command done!")
            elif cmd["command"] is ClientToServerCommand.GET_SOFTWARE_REPOSITORY:
                data_data = []
                for group in device.getGroups():
                    for rollout in group.getRollouts():
                        parameterstr = ""
                        for parameter in rollout.getApplication().getInstaller().getParameters():
                            parameterstr += parameter.getValue() + " "
                        rolloutdata = {"title": rollout.getApplication().getTitle(),
                                       "author": rollout.getApplication().getAuthor(),
                                       "wname": rollout.getApplication().getWName(),
                                       "wauthor": rollout.getApplication().getWAuthor(),
                                       "wnameformat": rollout.getApplication().getWNameFormat().getFormat(),
                                       "version": rollout.getVersion().getVersion(),
                                       "wversion": rollout.getVersion().getWVersion(),
                                       "path": rollout.getVersion().getPath(),
                                       "parameters": parameterstr
                                       }
                        data_data.append(rolloutdata)

                device.sendCommand({"command": ServerToClientCommand.RET_SOFTWARE_REPOSITORY,
                                    "data": data_data})

            else:
                print("Unknown command!")
    except (KeyError, TypeError) as e:
        Log.socket("%s sent an invalid package!" % (device.getName()))
        print(e)


class ServerToClientCommand:
    SET_STATUS_ON_CLIENT = 0
    RET_SOFTWARE_REPOSITORY = 1


class ClientToServerCommand:
    SEND_GENERAL_INFO_TO_SERVER = 0
    SEND_STATUS_INFO_TO_SERVER = 1
    GET_SOFTWARE_REPOSITORY = 2
