from classes import Storage


class Application:
    def __init__(self, data):
        self.id = data[0]
        self.title = data[1]
        self.author = data[2]
        self.wname = data[3]
        self.wauthor = data[4]
        self.wnameformat = Storage.wname_formats.get(data[5])
        self.installer = Storage.installers.get(data[6])
        self.versions = []
        for version in Storage.application_versions.values():
            if version.getApplicationID() == self.id:
                self.versions.append(version)

    def getID(self):
        return self.id

    def getTitle(self):
        return self.title

    def getAuthor(self):
        return self.author

    def getWName(self):
        return self.wname

    def getWAuthor(self):
        return self.wauthor

    def getWNameFormat(self):
        return self.wnameformat

    def getInstaller(self):
        return self.installer
