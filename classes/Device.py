from enum import Enum

from classes import DeviceHandler
from classes import MySQL
from classes import PackageUtil
from classes import Storage


class Device:

    def __init__(self, data_array):
        self.id = data_array[0]
        self.name = data_array[1]
        self.os = data_array[2]
        self.memory = data_array[4]
        self.architecture = data_array[3]
        self.domain = data_array[5]
        self.groups = []
        # add group classes
        for groupid in data_array[6].split(","):
            self.groups.append(Storage.groups.get(int(groupid)))

        self.connected = ConnectionStatus.DISCONNECTED
        # sockets
        self.queue = []

    def getID(self):
        return self.id

    def getName(self):
        return self.name

    def getOS(self):
        return self.os

    def getMemory(self):
        return self.memory

    def getConnectionStatus(self):
        return self.connected

    def getArchitecture(self):
        return self.architecture

    def getArchitectureInt(self):
        return self.architecture.value

    def getDomain(self):
        return self.domain

    def getGroups(self):
        return self.groups

    def setConnectionStatus(self, status):
        self.connected = status
        # clear queue because of connection status update
        self.queue = []
        return self

    def setOS(self, os):
        self.os = os
        return self

    def setArchitecture(self, arch):
        self.architecture = arch

    def setDeviceName(self, name):
        self.name = name

    def setMemory(self, memory):
        self.memory = memory

    def setDomain(self, domain):
        self.domain = domain

    def saveProperties(self):
        MySQL.saveDevice(self)

    def sendPackage(self, data):
        if self.connected == ConnectionStatus.DISCONNECTED:
            return
        self.queue.append(PackageUtil.getBytesFromJSON(PackageUtil.preparePackage(data)))

    def sendCommand(self, data):
        data["id"] = 0
        self.sendPackage({0: data})

    def shutdown(self):
        self.setStatus(1)

    def forceShutdown(self):
        self.forceSetStatus(1)

    def reboot(self):
        self.setStatus(2)

    def forceReboot(self):
        self.forceSetStatus(2)

    def forceGPUpdate(self):
        self.forceSetStatus(6)

    def test(self):
        # only for testings
        self.sendCommand({"command": DeviceHandler.ServerToClientCommand.SET_STATUS_ON_CLIENT,
                          "data": {"status": 7, "force": True}})

    def setStatus(self, statusid):
        self.sendCommand({"command": DeviceHandler.ServerToClientCommand.SET_STATUS_ON_CLIENT,
                          "data": {"status": statusid, "force": False}})

    def forceSetStatus(self, statusid):
        self.sendCommand({"command": DeviceHandler.ServerToClientCommand.SET_STATUS_ON_CLIENT,
                          "data": {"status": statusid, "force": True}})

    def update(self):
        self.setStatus(0)


class ConnectionStatus(Enum):
    CONNECTED = True
    DISCONNECTED = False


class DeviceArchitecture(Enum):
    x86 = 0,
    x64 = 1,
    arm64 = 2
