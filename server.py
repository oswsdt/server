#!/usr/bin/python
from _thread import *

from classes import Config
from classes import Console
from classes import Log
from classes import MySQL
from classes import Setup
from classes import SocketHandling
from classes import Storage

# config
CONFIG_FOLDER = './config/'
VERSION = 1
config = None

Log.CONFIG_FOLDER = CONFIG_FOLDER
Log.SAVE_LOGS = True


def fetch():
    # fetch all groups into dict
    Storage.parameters = MySQL.fetchAllParameters()
    # fetch all installers
    Storage.installers = MySQL.fetchAllInstallers()
    # fetch all formats into dict
    Storage.wname_formats = MySQL.fetchAllWNameFormats()
    # fetch all versions into dict
    Storage.application_versions = MySQL.fetchAllApplicationVersions()
    # fetch all applications into dict
    Storage.applications = MySQL.fetchAllApplications()
    # fetch all roll outs into dict
    Storage.rollouts = MySQL.fetchAllRollouts()
    # fetch all groups into dict
    Storage.groups = MySQL.fetchAllGroups()


def main():
    # main
    global config

    Log.info("Starting server(v" + str(VERSION) + ")...")
    Setup.setup()
    Log.info("Loading config.yml...")
    config = Config.loadConfig(CONFIG_FOLDER + "config.yml")
    Log.info("Connecting to MySQL...")
    MySQL.connect(config)

    fetch()

    Log.info("Binding socket on: %s" % str(config['port']))
    start_new_thread(SocketHandling.buildSocket, (config,))
    Log.info("Socket bound. Waiting for clients")

    while True:
        # do console stuff on main thread
        Console.rawInput(input())


if __name__ == '__main__':
    main()
