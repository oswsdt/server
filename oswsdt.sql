SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

CREATE TABLE `applications`
(
    `id`          int(11)      NOT NULL,
    `title`       varchar(256) NOT NULL,
    `author`      varchar(256) NOT NULL,
    `wname`       varchar(256) NOT NULL,
    `wauthor`     varchar(256) NOT NULL,
    `wnameformat` int(11)      NOT NULL,
    `installer`   int(11)      NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE `application_versions`
(
    `id`           int(11)                       NOT NULL,
    `application`  int(11)                       NOT NULL,
    `architecture` enum ('x86','x64','arm64','') NOT NULL,
    `version`      varchar(256)                  NOT NULL,
    `wversion`     varchar(256)                  NOT NULL,
    `path`         text                          NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;
CREATE TABLE `auth`
(
    `id`   int(11) NOT NULL,
    `cert` text    NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE `devices`
(
    `id`           int(11)                       NOT NULL,
    `name`         varchar(255)                  NOT NULL,
    `os`           varchar(256)                  NOT NULL,
    `architecture` enum ('x86','x64','arm64','') NOT NULL,
    `ram`          int(11)                       NOT NULL,
    `domain`       varchar(255)                  NOT NULL,
    `groups`       text                          NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;
CREATE TABLE `groups`
(

    `id`          int(11)      NOT NULL,
    `name`        varchar(256) NOT NULL,
    `description` text         NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE `installer`
(
    `id`         int(11)      NOT NULL,
    `name`       varchar(256) NOT NULL,
    `parameters` text         NOT NULL COMMENT 'id or parameter:id split by ","'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE `parameter`
(
    `id`          int(11)      NOT NULL,
    `name`        varchar(256) NOT NULL,
    `description` text         NOT NULL,
    `value`       text         NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE `rollouts`
(
    `id`          int(11) NOT NULL,
    `application` int(11) NOT NULL,
    `version`     int(11) NOT NULL,
    `groups`      text    NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE `wname_formats`
(
    `id`     int(11) NOT NULL,
    `format` text    NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

ALTER TABLE `applications`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `title` (`title`),
    ADD UNIQUE KEY `wname` (`wname`);

ALTER TABLE `application_versions`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `application` (`application`, `architecture`, `version`);

ALTER TABLE `auth`
    ADD PRIMARY KEY (`id`);

ALTER TABLE `devices`
    ADD PRIMARY KEY (`id`);

ALTER TABLE `groups`
    ADD PRIMARY KEY (`id`);

ALTER TABLE `installer`
    ADD PRIMARY KEY (`id`);

ALTER TABLE `parameter`
    ADD PRIMARY KEY (`id`);

ALTER TABLE `rollouts`
    ADD PRIMARY KEY (`id`);

ALTER TABLE `wname_formats`
    ADD PRIMARY KEY (`id`);


ALTER TABLE `applications`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 1;

ALTER TABLE `application_versions`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 1;

ALTER TABLE `auth`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 1;

ALTER TABLE `devices`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 1;

ALTER TABLE `groups`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 1;

ALTER TABLE `installer`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 1;

ALTER TABLE `parameter`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 1;

ALTER TABLE `rollouts`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 1;

ALTER TABLE `wname_formats`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 1;
COMMIT;
